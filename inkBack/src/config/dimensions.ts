export default {
  janela: {
    altura: 1.2,
    largura: 2,
  },
  porta: {
    altura: 1.9,
    largura: 0.8,
  },
};
