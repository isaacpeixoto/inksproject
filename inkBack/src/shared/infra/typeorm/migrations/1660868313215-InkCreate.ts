import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class InkCreate1660868313215 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "ink",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
          },
          {
            name: "0.5",
            type: "numeric",
          },
          {
            name: "2.5",
            type: "numeric",
          },
          {
            name: "3.6",
            type: "numeric",
          },
          {
            name: "18",
            type: "numeric",
          },
          {
            name: "totalArea",
            type: "numeric",
          },
          {
            name: "user_id",
            type: "uuid",
          },
          {
            name: "created_at",
            type: "timestamp",
            default: "now()",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("ink");
  }
}
