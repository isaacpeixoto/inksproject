import { Router } from "express";
import { authenticateRoutes } from "./authenticate.routes";
import { inkRoutes } from "./ink.routes";
import { usersRoutes } from "./users.routes";

const router = Router();

router.use(authenticateRoutes);
router.use("/users", usersRoutes);
router.use("/ink", inkRoutes);

export { router };
