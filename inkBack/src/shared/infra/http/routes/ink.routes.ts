import { Router } from "express";
import { CalculateInkController } from "../../../../modules/ink/usesCases/calculateInk/CreateInkController";
import { GetInkController } from "../../../../modules/ink/usesCases/getInk/GetInkController";
import { ensureAuthenticated } from "../middlewares/ensureAuthenticated";

const inkRoutes = Router();
const calculateInkController = new CalculateInkController();
const getInkController = new GetInkController();

inkRoutes.post("/", ensureAuthenticated, calculateInkController.handle);
inkRoutes.get("/", ensureAuthenticated, getInkController.handle);

export { inkRoutes };
