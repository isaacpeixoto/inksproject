import { container } from "tsyringe";
import { ICalculateProvider } from "./ICalculateProvider";
import { CalculateProvider } from "./implementations/CalculateProvider";

container.registerSingleton<ICalculateProvider>(
  "CalculateProvider",
  CalculateProvider
);
