import {
  ICalculateData,
  ICalculateProvider,
  ILitros,
} from "../ICalculateProvider";

class CalculateProvider implements ICalculateProvider {
  calculateArea({ altura, largura }: ICalculateData): number {
    const area = altura * largura;

    return area;
  }

  calculateInkLiter(area: number): ILitros {
    const calculateInkDivisor = area / 5;

    let litros = {
      "18.5": 0,
      "3.6": 0,
      "2.5": 0,
      "0.5": 0,
    };

    let tintasLitros = [18.5, 3.6, 2.5, 0.5];
    let inkTotal = calculateInkDivisor;

    for (var tintaLitro of tintasLitros) {
      while (inkTotal >= tintaLitro) {
        litros[tintaLitro] += 1;
        inkTotal -= tintaLitro;
      }
    }

    return litros;
  }
}

export { CalculateProvider };
