interface ICalculateData {
  altura: number;
  largura: number;
}

interface ILitros {
  "18.5": number;
  "3.6": number;
  "2.5": number;
  "0.5": number;
}

interface ICalculateProvider {
  calculateArea({ altura, largura }: ICalculateData): number;
  calculateInkLiter(area: number): ILitros;
}

export type { ICalculateProvider, ICalculateData, ILitros };
