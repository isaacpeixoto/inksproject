import { container } from "tsyringe";
import "../container/providers";
import { IInkRepositories } from "../../modules/ink/repositories/IInkRepositories";
import { InkRepository } from "../../modules/ink/infra/typeorm/repositories/InkRepository";
import { UsersRepository } from "../../modules/accounts/infra/typeorm/repositories/UsersRepository";
import { IUsersRepository } from "../../modules/accounts/repositories/IUsersRepository";

container.registerSingleton<IInkRepositories>("InkRepository", InkRepository);

container.registerSingleton<IUsersRepository>(
  "UsersRepository",
  UsersRepository
);
