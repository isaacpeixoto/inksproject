import { ICreateUserDTO } from "../../dtos/ICreateUserDTO";
import { User } from "../../infra/typeorm/entities/User";
import { IUsersRepository } from "../IUsersRepository";

class UsersRepositoryInMemory implements IUsersRepository {
  users: User[] = [];

  async create({ username, password }: ICreateUserDTO): Promise<void> {
    const user = new User();

    Object.assign(user, {
      username,
      password,
    });

    this.users.push(user);
  }

  async findByEmail(email: string): Promise<User> {
    return this.users.find((user) => user.email === email);
  }

  async findById(id: string): Promise<User> {
    return this.users.find((user) => user.id === id);
  }
  async findByUserName(username: string): Promise<User> {
    return this.users.find((user) => user.username === username);
  }
}

export { UsersRepositoryInMemory };
