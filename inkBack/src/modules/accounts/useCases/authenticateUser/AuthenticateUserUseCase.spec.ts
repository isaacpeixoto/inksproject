import { AppError } from "../../../../shared/errors/AppError";
import { ICreateUserDTO } from "../../dtos/ICreateUserDTO";
import { UsersRepositoryInMemory } from "../../repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "../createUser/CreateUserUseCase";
import { AuthenticateUserUseCase } from "./AuthenticateUserUseCase";

let authenticateUserUseCase: AuthenticateUserUseCase;
let usersRepositoryInMemory: UsersRepositoryInMemory;
let createUserUseCase: CreateUserUseCase;

describe("Authenticate User", () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();
    authenticateUserUseCase = new AuthenticateUserUseCase(
      usersRepositoryInMemory
    );
    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
  });

  it("deve ser capaz de autenticar um usuário", async () => {
    const user: ICreateUserDTO = {
      username: "test",
      password: "1234",
    };

    await createUserUseCase.execute(user);

    const result = await authenticateUserUseCase.execute({
      username: user.username,
      password: user.password,
    });

    expect(result).toHaveProperty("token");
  });

  it("não deve ser capaz de autenticar um usuário inexistente", async () => {
    await expect(
      authenticateUserUseCase.execute({
        username: "falseuser",
        password: "1234",
      })
    ).rejects.toEqual(new AppError("Username or password incorrect!"));
  });

  it("não deve ser capaz de autenticar com senha incorreta", async () => {
    const user: ICreateUserDTO = {
      username: "usetest",
      password: "1234",
    };
    await createUserUseCase.execute(user);
    await expect(
      authenticateUserUseCase.execute({
        username: user.username,
        password: "incorretPassword",
      })
    ).rejects.toEqual(new AppError("Username or password incorrect!"));
  });
});
