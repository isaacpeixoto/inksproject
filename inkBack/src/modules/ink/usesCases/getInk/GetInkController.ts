import { Request, Response } from "express";
import { container } from "tsyringe";
import { GetInkUseCase } from "./GetInkUseCase";

class GetInkController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;

    const getInkByUserUseCase = container.resolve(GetInkUseCase);

    const ink = await getInkByUserUseCase.execute(id);

    return response.json(ink);
  }
}

export { GetInkController };
