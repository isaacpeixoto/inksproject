import { inject, injectable } from "tsyringe";
import { Ink } from "../../infra/typeorm/entities/Ink";
import { IInkRepositories } from "../../repositories/IInkRepositories";

@injectable()
class GetInkUseCase {
  constructor(
    @inject("InkRepository")
    private calculateRepository: IInkRepositories
  ) {}

  async execute(user_id: string): Promise<Ink[]> {
    const inksByUser = await this.calculateRepository.findById(user_id);
    return inksByUser;
  }
}

export { GetInkUseCase };
