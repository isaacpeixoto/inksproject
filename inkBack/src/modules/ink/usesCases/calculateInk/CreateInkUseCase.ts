import { inject, injectable } from "tsyringe";
import dimensions from "../../../../config/dimensions";
import { ICalculateProvider } from "../../../../shared/container/providers/CalculateProvider/ICalculateProvider";
import { AppError } from "../../../../shared/errors/AppError";
import { IInkRepositories } from "../../repositories/IInkRepositories";

interface IStructure {
  altura: number;
  largura: number;
  janela: number;
  porta: number;
}

interface IRequest {
  id: string;
  inksLiter: {
    "18": number;
    "3.6": number;
    "2.5": number;
    "0.5": number;
  };
  totalArea: number;
  created_at: Date;
}

@injectable()
class CreateInkUseCase {
  constructor(
    @inject("InkRepository")
    private calculateRepository: IInkRepositories,
    @inject("CalculateProvider")
    private calculateProvider: ICalculateProvider
  ) {}

  async execute(medidas: IStructure[], user_id): Promise<IRequest> {
    const quantidadeParede = medidas.length;

    if (quantidadeParede > 4) {
      throw new AppError("O máximo de paredes possíveis são 4");
    }

    let areaCalculator = [];
    for (let i = 0; i < quantidadeParede; i++) {
      const paredeArea = this.calculateProvider.calculateArea({
        altura: medidas[i].altura,
        largura: medidas[i].largura,
      });

      const paredeAreaDivisao50Porcento = paredeArea / 2;

      if (paredeArea < 1 || paredeArea > 50) {
        throw new AppError(
          "As medidas estão abaixo do minimo de 1m² ou acima do máximo de 50m²"
        );
      }

      let janelaArea = this.calculateProvider.calculateArea({
        altura: dimensions.janela.altura,
        largura: dimensions.janela.largura,
      });

      let janelasArea = medidas[i].janela * janelaArea;

      if (medidas[i].porta > 0 && medidas[i].altura < 2.2) {
        throw new AppError(
          "As parede com portas devem ser pelo menos 30cm maior que a porta."
        );
      }

      let portaArea = this.calculateProvider.calculateArea({
        altura: dimensions.porta.altura,
        largura: dimensions.porta.largura,
      });

      let portasArea = medidas[i].porta * portaArea;

      if (
        janelasArea > paredeAreaDivisao50Porcento ||
        portasArea > paredeAreaDivisao50Porcento
      ) {
        throw new AppError(
          "A janela ou a porta possuem mais de 50% da area da parede, por favor tente um valor menor."
        );
      }

      let areaTotal = (paredeArea - janelasArea - portasArea).toFixed(2) as
        | string
        | number;

      areaTotal = Number(areaTotal as string);

      areaCalculator.push(areaTotal);
    }

    let areaFinal = areaCalculator.reduce((a, acc) => {
      return a + acc;
    }, 0);

    areaFinal = Math.round(areaFinal);

    const latasLitroNecessario =
      this.calculateProvider.calculateInkLiter(areaFinal);

    const inkResult = await this.calculateRepository.create({
      meioLitro: latasLitroNecessario["0.5"],
      doisLitrosEMeio: latasLitroNecessario["2.5"],
      tresLitrosEMeio: latasLitroNecessario["3.6"],
      dezoitoLitros: latasLitroNecessario["18.5"],
      user_id,
      totalArea: areaFinal,
    });

    const inkResultFormat = {
      id: inkResult.id,
      inksLiter: {
        "18": latasLitroNecessario["18.5"],
        "3.6": latasLitroNecessario["3.6"],
        "2.5": latasLitroNecessario["2.5"],
        "0.5": latasLitroNecessario["0.5"],
      },
      totalArea: inkResult.totalArea,
      created_at: inkResult.created_at,
    };

    return inkResultFormat;
  }
}

export { CreateInkUseCase };
