import { Request, Response } from "express";
import { container } from "tsyringe";
import { CreateInkUseCase } from "./CreateInkUseCase";

class CalculateInkController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { medidas } = request.body;
    const { id } = request.user;

    const calculateInkUseCase = container.resolve(CreateInkUseCase);

    const inkResult = await calculateInkUseCase.execute(medidas, id);

    return response.json(inkResult);
  }
}

export { CalculateInkController };
