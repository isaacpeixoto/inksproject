import { CalculateProvider } from "../../../../shared/container/providers/CalculateProvider/implementations/CalculateProvider";
import { AppError } from "../../../../shared/errors/AppError";
import { InkRepositoryInMemory } from "../../repositories/in-memory/InkRepositoryInMemory";
import { CreateInkUseCase } from "./CreateInkUseCase";

let calculateProvider: CalculateProvider;
let createInkRepositoryInMemory: InkRepositoryInMemory;
let createInkUseCase: CreateInkUseCase;

describe("Create Ink", () => {
  beforeEach(() => {
    createInkRepositoryInMemory = new InkRepositoryInMemory();
    calculateProvider = new CalculateProvider();
    createInkUseCase = new CreateInkUseCase(
      createInkRepositoryInMemory,
      calculateProvider
    );
  });

  const user_id = "501d5888-1fe4-11ed-861d-0242ac120002";

  it("deve ser capaz de criar um novo calculo", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 5,
        largura: 5,
        janela: 0,
        porta: 1,
      },
    ];
    const inksCreate = await createInkUseCase.execute(medidas, user_id);

    expect(inksCreate).toHaveProperty("id");
  });

  it("Não deve ser possível criar medidas com mais de 4 paredes", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 5,
        largura: 5,
        janela: 0,
        porta: 1,
      },
      {
        altura: 5,
        largura: 5,
        janela: 0,
        porta: 1,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError("O máximo de paredes possíveis são 4")
    );
  });

  it("Não deve ser possível criar medidas com menos de 1m2", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 1,
        largura: 0.8,
        janela: 0,
        porta: 0,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError(
        "As medidas estão abaixo do minimo de 1m² ou acima do máximo de 50m²"
      )
    );
  });

  it("Não deve ser possível criar medidas com acima de 50m2", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 11,
        largura: 11,
        janela: 0,
        porta: 0,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError(
        "As medidas estão abaixo do minimo de 1m² ou acima do máximo de 50m²"
      )
    );
  });

  it("Não deve ser possível criar calculo com paredes que possuam portas e não sejam pelo menos 30cm maior que a altura da parede", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 1.9,
        largura: 6,
        janela: 0,
        porta: 1,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError(
        "As parede com portas devem ser pelo menos 30cm maior que a porta."
      )
    );
  });

  it("A janela não pode ter mais que 50% da area total da parede", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 5,
        largura: 6,
        janela: 8,
        porta: 1,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError(
        "A janela ou a porta possuem mais de 50% da area da parede, por favor tente um valor menor."
      )
    );
  });

  it("A porta não pode ter mais que 50% da area total da parede", async () => {
    const medidas = [
      {
        altura: 7,
        largura: 5,
        janela: 0,
        porta: 0,
      },
      {
        altura: 7,
        largura: 7,
        janela: 2,
        porta: 0,
      },
      {
        altura: 6,
        largura: 6,
        janela: 2,
        porta: 1,
      },
      {
        altura: 5,
        largura: 6,
        janela: 0,
        porta: 10,
      },
    ];

    await expect(createInkUseCase.execute(medidas, user_id)).rejects.toEqual(
      new AppError(
        "A janela ou a porta possuem mais de 50% da area da parede, por favor tente um valor menor."
      )
    );
  });
});
