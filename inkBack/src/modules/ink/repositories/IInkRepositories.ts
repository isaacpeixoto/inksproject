import { ICreateInkDTO } from "../dtos/ICreateInkDTO";
import { Ink } from "../infra/typeorm/entities/Ink";

interface IInkRepositories {
  create({
    meioLitro,
    doisLitrosEMeio,
    tresLitrosEMeio,
    dezoitoLitros,
    user_id,
    totalArea,
  }: ICreateInkDTO): Promise<Ink>;
  findById(id: string): Promise<Ink[]>;
}

export type { IInkRepositories };
