import { ICreateInkDTO } from "../../dtos/ICreateInkDTO";
import { Ink } from "../../infra/typeorm/entities/Ink";
import { IInkRepositories } from "../IInkRepositories";

class InkRepositoryInMemory implements IInkRepositories {
  inksMemory: Ink[] = [];

  async create({
    meioLitro,
    doisLitrosEMeio,
    tresLitrosEMeio,
    dezoitoLitros,
    totalArea,
  }: ICreateInkDTO): Promise<Ink> {
    const inkMemory = new Ink();

    Object.assign(inkMemory, {
      meioLitro,
      doisLitrosEMeio,
      tresLitrosEMeio,
      dezoitoLitros,
      totalArea,
      created_at: new Date(),
    });

    this.inksMemory.push(inkMemory);

    return inkMemory;
  }

  async findById(id: string): Promise<Ink[]> {
    return this.inksMemory.filter((ink) => ink.user_id === id);
  }
}

export { InkRepositoryInMemory };
