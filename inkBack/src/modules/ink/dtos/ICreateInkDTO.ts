interface ICreateInkDTO {
  meioLitro: number;
  doisLitrosEMeio: number;
  tresLitrosEMeio: number;
  dezoitoLitros: number;
  user_id: string;
  totalArea: number;
}

export type { ICreateInkDTO };
