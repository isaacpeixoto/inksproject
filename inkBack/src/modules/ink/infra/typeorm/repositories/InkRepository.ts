import { getRepository, Repository } from "typeorm";
import { ICreateInkDTO } from "../../../dtos/ICreateInkDTO";
import { IInkRepositories } from "../../../repositories/IInkRepositories";
import { Ink } from "../entities/Ink";

class InkRepository implements IInkRepositories {
  private repository: Repository<Ink>;

  constructor() {
    this.repository = getRepository(Ink);
  }
  async create({
    meioLitro,
    doisLitrosEMeio,
    tresLitrosEMeio,
    dezoitoLitros,
    user_id,
    totalArea,
  }: ICreateInkDTO): Promise<Ink> {
    const ink = this.repository.create({
      "0.5": meioLitro,
      "2.5": doisLitrosEMeio,
      "3.6": tresLitrosEMeio,
      "18": dezoitoLitros,
      user_id,
      totalArea,
    });

    await this.repository.save(ink);

    return ink;
  }

  async findById(user_id: string): Promise<Ink[]> {
    const ink = await this.repository.find({
      where: { user_id },
    });

    return ink;
  }
}

export { InkRepository };
