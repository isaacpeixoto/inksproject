import { Column, CreateDateColumn, Entity, PrimaryColumn } from "typeorm";
import { v4 as uuidV4 } from "uuid";

@Entity("ink")
class Ink {
  @PrimaryColumn()
  id: string;

  @Column()
  "0.5": number;

  @Column()
  "2.5": number;

  @Column()
  "3.6": number;

  @Column()
  "18": number;

  @Column()
  totalArea: number;

  @Column()
  user_id: string;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuidV4();
    }
  }
}

export { Ink };
