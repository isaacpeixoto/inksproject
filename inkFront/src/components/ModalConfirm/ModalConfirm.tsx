import {
  BodyModal,
  Overlay,
  Close,
  Container,
  ButtonContainerConfirm,
} from './style';

interface IDate {
  created_at: string;
  id: string;
  inksLiter: { 18: number; 3.6: number; 2.5: number; 0.5: number };
  totalArea: number;
}
interface IModal {
  setModal: (args: boolean) => void;
  modal: boolean;
  data: IDate;
}

export function ModalConfirm({ setModal, modal, data }: IModal): any {
  function handleClick(e: any) {
    if (e.currentTarget === e.target) {
      setModal(false);
    }
  }
  if (modal)
    return (
      <Overlay onClick={handleClick}>
        <Container>
          <div>
            <Close>
              <button
                onClick={() => {
                  setModal(false);
                }}
                type="button"
              >
                <svg width="30" height="30" viewBox="0 0 20 20" fill="##875EFF">
                  <path
                    d="M4.35156 5.19496L9.15406 9.99746L4.35156 14.8L5.20009 15.6485L10.0026 10.846L14.7963 15.6397L15.6449 14.7912L10.8511 9.99746L15.6449 5.20371L14.7963 4.35518L10.0026 9.14894L5.20009 4.34644L4.35156 5.19496Z"
                    fill="#fd0f04"
                    stroke="#fd0f04"
                  ></path>
                </svg>
              </button>
            </Close>
            <BodyModal>
              <div className="infos">
                <h1>
                  Resultado da quantidade de latas de tintas necessárias para
                  pintar {data.totalArea}m²:
                </h1>
                {data.inksLiter[18] ? (
                  <p>Você precisará de {data.inksLiter[18]} latas de 18 L</p>
                ) : null}
                {data.inksLiter['3.6'] ? (
                  <p>
                    Você precisará de {data.inksLiter['3.6']} latas de 3.6 L
                  </p>
                ) : null}
                {data.inksLiter['2.5'] ? (
                  <p>
                    Você precisará de {data.inksLiter['2.5']} latas de 2.5 L
                  </p>
                ) : null}
                {data.inksLiter['0.5'] ? (
                  <p>
                    Você precisará de {data.inksLiter['0.5']} latas de 0.5 L
                  </p>
                ) : null}
              </div>
              <ButtonContainerConfirm>
                <button
                  onClick={() => {
                    setModal(false);
                  }}
                  type="button"
                >
                  ENTENDIDO
                </button>
              </ButtonContainerConfirm>
            </BodyModal>
          </div>
        </Container>
      </Overlay>
    );
  else return null;
}
