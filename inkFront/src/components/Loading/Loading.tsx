import Lottie from 'react-lottie';
import { Container } from './style';
import Paint from '../../assets/images/paint.json';

const Loading = () => {
  const options = {
    renderer: 'svg',

    loop: true,
    autoplay: true,
    animationData: Paint,
  };
  return (
    <Container>
      <Lottie options={options} width={150} speed={2} isClickToPauseDisabled />
    </Container>
  );
};

export default Loading;
