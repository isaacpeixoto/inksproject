import styled from 'styled-components';
export const Container = styled.div`
  max-width: 1200px;
  width: 95%;
  margin: 0 auto;
  text-align: center;
  margin-bottom: 32px;

  h1 {
    color: ${({ theme }) => theme.colors.blackColorContraste};
    margin-bottom: 0px;
    font-size: 22px;
    margin: 22px 0px;
    text-transform: uppercase;
  }

  /* CELULAR */
  @media (max-width: 500px) {
    label {
      font-size: 14px;
      flex-direction: row;

      img {
        margin-bottom: 0px;
      }
      p {
        margin-bottom: 0px;
      }
    }
  }
`;

export const CardInkCalculate = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  color: #fff;

  form {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    justify-content: space-between;
    .card {
      width: 48%;
      display: flex;
      flex-direction: column;
      padding: 25px 20px 30px 20px;
      background-color: ${({ theme }) => theme.colors.blackColorContraste};
      margin-bottom: 32px;
      border-radius: 8px;

      label {
        cursor: pointer;
        padding: 10px 5px;
        display: flex;
        width: 100%;
        font-weight: bold;
        font-size: 18px;
        border-radius: 6px;
        img {
          margin-bottom: 12px;
        }
        p {
          margin-bottom: 12px;
        }
      }
      input {
        padding: 10px;
        border-radius: 6px;
        border: 1px solid transparent;
        outline: none;
        font-size: 16px;
        width: 100%;
      }
      input::-webkit-outer-spin-button,
      input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }
      input[type='number'] {
        -moz-appearance: textfield;
      }
    }
  }
  img {
    width: 60px;
    border-radius: 8px;
  }

  .adicionar-card {
    border: 1px solid transparent;
    background-color: #4cef79;
    margin-right: 16px;
    margin-bottom: 32px;
    padding: 10px;
    border-radius: 10px;
    width: 60px;
    height: 60px;
    box-shadow: 4px 4px 14px rgba(0, 0, 0, 0.2);
  }
  .remover-card {
    border: 1px solid transparent;
    background-color: #f86565;
    padding: 10px;
    width: 62px;
    height: 62px;
    border-radius: 10px;
    box-shadow: 4px 4px 14px rgba(0, 0, 0, 0.2);
  }

  @media (max-width: 500px) {
    form {
      flex-direction: column;
      .card {
        width: 100%;
      }
    }
  }
`;
export const ServiceCardCalculateInk = styled.div`
  margin: 0 auto;
  margin-top: 42px;
  width: 250px;
  border-radius: 10px;
  box-shadow: 8px 8px 14px rgba(0, 0, 0, 0.3);
  button {
    height: 60px;
    font-weight: bold;
    font-size: 22px;
  }
`;
