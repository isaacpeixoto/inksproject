import React, { useState } from 'react';
import { toast } from 'react-toastify';
import CreateInkService, {
  IInkRequest,
} from '../../services/CalculateInk/CreateInkService';
import Button from '../Button/Button';
import { ModalConfirm } from '../ModalConfirm/ModalConfirm';
import { ReactComponent as Add } from '../../assets/images/add.svg';
import { ReactComponent as Remove } from '../../assets/images/remove.svg';
import { CardInkCalculate, Container, ServiceCardCalculateInk } from './style';

interface IMedidas {
  altura: number;
  largura: number;
  janela: number;
  porta: number;
}

interface IDate {
  created_at: string;
  id: string;
  inksLiter: { 18: number; 3.6: number; 2.5: number; 0.5: number };
  totalArea: number;
}

const CardCalculo = () => {
  const [modal, setModal] = useState<boolean>(false);
  const [verify, setVerify] = useState(false);
  const [data, setData] = useState<IDate | IInkRequest>();
  const [medidas, setMedidas] = useState<IMedidas[]>([
    {
      altura: 0,
      largura: 0,
      janela: 0,
      porta: 0,
    },
  ]);

  function handleAddPorta() {
    if (medidas.length < 4) {
      setMedidas((prevState: IMedidas[]) => [
        ...prevState,
        {
          altura: 0,
          largura: 0,
          janela: 0,
          porta: 0,
        },
      ]);
    }
  }

  function handleRemoverPorta() {
    if (medidas.length > 1) {
      setMedidas(medidas.filter((item, index) => index !== medidas.length - 1));
    }
  }

  function handleChangeAltura(
    e: React.ChangeEvent<HTMLInputElement>,
    index: number,
  ) {
    let medidasUpdate = [...medidas];
    medidasUpdate[index].altura = Number(e.target.value);
    !medidasUpdate[index].altura ? setVerify(false) : setVerify(true);
    setMedidas(medidasUpdate);
  }

  function handleChangeLargura(
    e: React.ChangeEvent<HTMLInputElement>,
    index: number,
  ) {
    let medidasUpdate = [...medidas];
    medidasUpdate[index].largura = Number(e.target.value);
    !medidasUpdate[index].largura ? setVerify(false) : setVerify(true);
    setMedidas(medidasUpdate);
  }
  function handleChangeJanela(
    e: React.ChangeEvent<HTMLInputElement>,
    index: number,
  ) {
    let medidasUpdate = [...medidas];

    medidasUpdate[index].janela = Number(e.target.value);
    setMedidas(medidasUpdate);
  }

  function handleChangePorta(
    e: React.ChangeEvent<HTMLInputElement>,
    index: number,
  ) {
    let medidasUpdate = [...medidas];
    medidasUpdate[index].porta = Number(e.target.value);
    setMedidas(medidasUpdate);
  }

  async function handleInputChange() {
    const token = localStorage.getItem('token');
    if (token) {
      const { json, response } = await CreateInkService.calculate(
        medidas,
        token,
      );
      const message = json.message;

      if (response.status === 400) {
        toast.error(message, {
          position: 'top-right',
          autoClose: 7000,
          hideProgressBar: false,
          closeOnClick: true,
          draggable: true,
          progress: undefined,
        });
        return;
      }

      toast.success('Medidas calculadas com sucesso', {
        position: 'top-right',
        autoClose: 7000,
        hideProgressBar: false,
        closeOnClick: true,
        draggable: true,
        progress: undefined,
      });

      setModal(true);
      setData(json as IDate | IInkRequest);
      return json;
    }
  }

  return (
    <Container>
      <CardInkCalculate>
        <ModalConfirm data={data as IDate} setModal={setModal} modal={modal} />
        <h1>Adicione ou remova paredes:</h1>
        <button className="adicionar-card" onClick={handleAddPorta}>
          <Add />
        </button>
        <button className="remover-card" onClick={handleRemoverPorta}>
          <Remove />{' '}
        </button>
        <form>
          {medidas.map((medida: IMedidas, index: number) => (
            <div className="card" key={index}>
              <h2>Parede {index + 1}</h2>
              <label htmlFor="altura">Altura</label>
              <input
                type="number"
                id="altura"
                onChange={(e) => {
                  handleChangeAltura(e, index);
                }}
              />

              <label htmlFor="largura">Largura</label>
              <input
                type="number"
                id="largura"
                onChange={(e) => {
                  handleChangeLargura(e, index);
                }}
              />

              <label htmlFor="janlea">Janela</label>
              <input
                type="number"
                id="janlea"
                onChange={(e) => {
                  handleChangeJanela(e, index);
                }}
              />

              <label htmlFor="porta">Porta</label>
              <input
                type="number"
                id="porta"
                onChange={(e) => {
                  handleChangePorta(e, index);
                }}
              />
            </div>
          ))}
        </form>
        <ServiceCardCalculateInk>
          <Button disabled={!verify} onClick={handleInputChange}>
            CALCULAR
          </Button>
        </ServiceCardCalculateInk>
      </CardInkCalculate>
    </Container>
  );
};

export default CardCalculo;
