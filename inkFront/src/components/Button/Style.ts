import styled from 'styled-components';

export const Container = styled.button`
  border-radius: 8px;
  width: 100%;
  height: 50px;
  border: none;
  font-size: 18px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.colors.background};
  background: ${({ theme }) => theme.colors.blackColorContraste};
  transition: background 0.5s ease 0s, color 0.5s ease 0s;

  &[disabled] {
    background-color: rgba(25, 45, 48, 0.34);
    color: rgba(25, 45, 48, 0.34);
    cursor: not-allowed;
    &:hover {
      background-color: rgba(82, 85, 94, 0.31);
    }
  }
  &:hover {
    color: ${({ theme }) => theme.colors.background};
    background-color: ${({ theme }) => theme.colors.blackColorContraste};
  }

  /* CELULAR */
  @media (max-width: 480px) {
    height: 40px;
    font-size: 18px;
  }
`;
