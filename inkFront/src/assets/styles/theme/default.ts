import { DefaultTheme } from 'styled-components';

export const theme: DefaultTheme = {
  colors: {
    background: '#CDDEF8',
    backgroundSecundary: '#2F68D8',
    backgroundForms: '#F6F5FC',
    textPrimary: '#333',
    textFormsPlaceHolder: '#BCBCBC',
    errorColor: '#FA0553',
    blackColorContraste: '#2D2D30',
    backgroundContr: '#DEE2EE',
  },
};
