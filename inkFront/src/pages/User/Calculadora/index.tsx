import Head from '../../../helper/Head';
import { Container } from './style';
import { useCallback, useEffect, useState } from 'react';
import Loading from '../../../components/Loading/Loading';
import CardCalculo from '../../../components/CardCalculo';
import GetInkService from '../../../services/GetInk/GetInkService';

interface IRequest {
  0.5: string;
  2.5: string;
  3.6: string;
  18: string;
  created_at: string;
  id: string;
  totalArea: string;
  user_id: string;
}

const Calculadora = () => {
  const [loading, setLoading] = useState(true);
  const [data, setDate] = useState<IRequest[]>([]);
  data.length = 10

  setTimeout(() => {
    setLoading(false);
  }, 2000);

  const getHistory = useCallback(async () => {
    const token = localStorage.getItem('token');
    if (token) {
      const { json } = await GetInkService.get(token);
      setDate(json);
    }
  }, []);

  useEffect(() => {
    getHistory();
  }, [getHistory]);

  if (loading) return <Loading />;
  if (data)
    return (
      <Container>
        <Head title="Calculadora" />
        <CardCalculo />

        <h1 className="title-ultimo--calculo">Ultimos Calculos:</h1>
        {data.map((item) => (
          <div key={item.id} className="subcontainer">
            <div>
              <div className="ultimos-calculos--card">
                <p>
                  <strong>Total da area para pintar: </strong> {item.totalArea}
                </p>
                <p>
                  <strong>18L:</strong> {item[18]} Litros
                </p>
                <p>
                  <strong>3.6L:</strong> {item['3.6']} Litros
                </p>{' '}
                <p>
                  <strong>2.5L:</strong> {item['2.5']} Litros
                </p>{' '}
                <p>
                  <strong>0.5L:</strong> {item['0.5']} Litros
                </p>
              </div>
            </div>
          </div>
        ))}
        <hr />
      </Container>
    );
  else return null;
};

export default Calculadora;

