import styled from 'styled-components';

export const Container = styled.div`
  max-width: 1200px;
  width: 95%;
  margin: 0 auto;
  margin-top: 62px;
  text-align: center;
  padding-bottom: 32px;
  h1 {
    color: ${({ theme }) => theme.colors.blackColorContraste};
    margin-bottom: 20px;
    font-size: 22px;
    text-transform: uppercase;
  }

  hr {
    border: 1px solid rgba(0, 0, 0, 0.2);
    margin-bottom: 20px;
  }
  .ultimos-calculos--card {
    background: ${({ theme }) => theme.colors.textPrimary};
    box-shadow: 4px 4px 14px rgba(0, 0, 0, 0.2);
    margin-bottom: 12px;
    padding: 20px 20px;
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: ${({ theme }) => theme.colors.background};
    border-radius: 8px;
    font-size: 14px;
    img {
      border-radius: 5%;
      width: 40px;
    }
  }
  .title-ultimo--calculo {
    margin-top: 62px;
  }

  @media (max-width: 500px) {
    font-size: 12px;
    flex-direction: column;
    h1 {
      font-size: 18px;
    }

    .ultimos-calculos--card {
      padding: 20px 9px;
      text-align: center;
      img {
        width: 60px;
        height: 60px;
        margin-right: 12px;
      }
    }
  }
`;
