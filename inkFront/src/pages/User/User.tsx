import { Route, Routes } from 'react-router-dom';
import Calculadora from './Calculadora';
import Header from '../../components/Header';
import NotFound from '../Notfound/NotFound';

const User = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/calculadora" element={<Calculadora />} />
        <Route path='/*' element={<NotFound />} />

      </Routes>
    </>
  );
};

export default User;
