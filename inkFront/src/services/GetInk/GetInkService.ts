import { HttpClient } from './utils/HttpClient';

interface IRequest {
  0.5: string;
  2.5: string;
  3.6: string;
  18: string;
  created_at: string;
  id: string;
  totalArea: string;
  user_id: string;
}

interface IRequest {
  json: IRequest[];
  response: Response;
}

class GetInkService {
  private httpClient: InstanceType<new (...args: []) => any>;
  constructor() {
    this.httpClient = new HttpClient(`http://localhost:3333`);
  }

  async get(token: string): Promise<IRequest> {
    return await this.httpClient.get(`/ink`, token);
  }
}

export default new GetInkService();
