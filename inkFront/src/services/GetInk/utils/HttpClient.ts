class HttpClient {
  constructor(private baseUrl: string) {}
  async get(path: string, token: string) {
    try {
      const response = await fetch(`${this.baseUrl}${path}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Beare ' + token,
        },
      });

      const json = await response.json();

      return { json, response };
    } catch (err) {
      console.log(err);
    }
  }
}

export { HttpClient };
