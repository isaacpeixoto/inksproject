interface IInkRequest {
  altura: number;
  largura: number;
  janela: number;
  porta: number;
}
class HttpClient {
  constructor(private baseUrl: string) {}
  async post(path: string, body: IInkRequest[], token: string) {
    try {
      const response = await fetch(`${this.baseUrl}${path}`, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Beare ' + token,
        },
      });

      const json = await response.json();

      return { json, response };
    } catch (err) {
      console.log(err);
    }
  }
}

export { HttpClient };
