import { HttpClient } from './utils/HttpClient';

export interface IInkRequest {
  altura: number;
  largura: number;
  janela: number;
  porta: number;
  message?: string;
}

interface IRequest {
  json: IInkRequest;
  response: Response;
}

class CreateUserService {
  private httpClient: InstanceType<new (...args: []) => any>;
  constructor() {
    this.httpClient = new HttpClient(`http://localhost:3333`);
  }

  async calculate(medidas: IInkRequest[], token: string): Promise<IRequest> {
    return await this.httpClient.post(`/ink`, { medidas }, token);
  }
}

export default new CreateUserService();
